# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class PartyUserGroupTestCase(ModuleTestCase):
    """Test Party User Group module"""
    module = 'party_user_group'


del ModuleTestCase
