datalife_party_user_group
=========================

The party_user_group module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-party_user_group/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-party_user_group)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
